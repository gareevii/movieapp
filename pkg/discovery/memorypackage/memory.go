package memory

import (
	"context"
	"errors"
	"sync"
	"time"

	"movieexample.com/pkg/discovery"
)

type serviceName string
type instanceID string

// Registry defines an in-memory service registry.
type Registry struct {
	sync.RWMutex
	serviceAddrs map[serviceName]map[instanceID]*serviceInstance
}

type serviceInstance struct {
	hostPort   string
	lastActive time.Time
}

// NewRegistry creates a new in-memory service registry instance.
func NewRegistry() *Registry {
	return &Registry{serviceAddrs: map[serviceName]map[instanceID]*serviceInstance{}}
}

// Register creates a service record in the registry.
func (r *Registry) Register(ctx context.Context, instID string, servName string, hostPort string) error {
	r.Lock()
	defer r.Unlock()
	if _, ok := r.serviceAddrs[serviceName(servName)]; !ok {
		r.serviceAddrs[serviceName(servName)] = map[instanceID]*serviceInstance{}
	}
	r.serviceAddrs[serviceName(servName)][instanceID(instID)] = &serviceInstance{hostPort: hostPort, lastActive: time.Now()}
	return nil
}

// Deregister removes a service record from the registry.
func (r *Registry) Deregister(ctx context.Context, instID string, servName string) error {
	r.Lock()
	defer r.Unlock()
	if _, ok := r.serviceAddrs[serviceName(servName)]; !ok {
		return nil
	}
	delete(r.serviceAddrs[serviceName(servName)], instanceID(instID))
	return nil
}

// ReportHealthyState is a push mechanism for reporting healthy state to the registry.
func (r *Registry) ReportHealthyState(instID string, servName string) error {
	r.Lock()
	defer r.Unlock()
	if _, ok := r.serviceAddrs[serviceName(servName)]; !ok {
		return errors.New("service is not registered yet")
	}
	if _, ok := r.serviceAddrs[serviceName(servName)][instanceID(instID)]; !ok {
		return errors.New("service instance is not registered yet")
	}
	r.serviceAddrs[serviceName(servName)][instanceID(instID)].lastActive = time.Now()
	return nil
}

// ServiceAddresses returns the list of addresses of active instances of the given service.
func (r *Registry) ServiceAddresses(ctx context.Context, servName string) ([]string, error) {
	r.RLock()
	defer r.RUnlock()
	servAddr := r.serviceAddrs[serviceName(servName)]
	if len(servAddr) == 0 {
		return nil, discovery.ErrNotFound
	}
	var res []string
	for _, i := range r.serviceAddrs[serviceName(servName)] {
		// TODO: connection to services will lose after 5 seconds, unnecessary peace of code
		/*if i.lastActive.Before(time.Now().Add(-5 * time.Second)) {
			continue
		}*/
		res = append(res, i.hostPort)
	}
	return res, nil
}
