package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"google.golang.org/grpc"
	"gopkg.in/yaml.v3"
	"movieexample.com/gen"
	"movieexample.com/metadata/internal/controller/metadata"
	grpchandler "movieexample.com/metadata/internal/handler/grpc"
	"movieexample.com/metadata/internal/repository/memory"
	"movieexample.com/pkg/discovery"
	memoryRegistry "movieexample.com/pkg/discovery/memorypackage"
)

func main() {
	//f, err := os.Open("configs/base.yaml")
	f, err := os.Open("base.yaml")
	if err != nil {
		panic(err)
	}
	var cfg config
	if err := yaml.NewDecoder(f).Decode(&cfg); err != nil {
		panic(err)
	}
	serviceName, host, port := cfg.Service.Name, cfg.API.Host, cfg.API.Port
	log.Printf("Starting the \"metadata\" service on port: %d\n", port)

	//TODO: replaced due kubernetes (consul container have to setup inside kubernetes)
	registry := memoryRegistry.NewRegistry()
	/*consulHost, consulPort := cfg.Register.Consul.Host, cfg.Register.Consul.Port
	registry, err := consul.NewRegistry(fmt.Sprintf("%s:%d", consulHost, consulPort))
	if err != nil {
		panic(err)
	}*/

	ctx := context.Background()
	instanceID := discovery.GenerateInstanceID(serviceName)
	if err := registry.Register(ctx, instanceID, serviceName, fmt.Sprintf("%s:%d", host, port)); err != nil {
		panic(err)
	}
	go func() {
		for {
			if err := registry.ReportHealthyState(instanceID, serviceName); err != nil {
				log.Println("Failed to report healthy state: " + err.Error())
			}
			time.Sleep(1 * time.Second)
		}
	}()
	defer registry.Deregister(ctx, instanceID, serviceName)

	//TODO: replaced due kubernetes (mysql container have to setup inside kubernetes)
	/*repo := mysql.New()
	if err != nil {
		panic(err)
	}*/
	repo := memory.New()

	ctrl := metadata.New(repo)
	h := grpchandler.New(ctrl)
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	srv := grpc.NewServer()
	gen.RegisterMetadataServiceServer(srv, h)
	if err := srv.Serve(lis); err != nil {
		panic(err)
	}
}
