package memory

import (
	"context"
	"sync"

	"movieexample.com/metadata/internal/repository"
	"movieexample.com/metadata/pkg/model"
)

type Repository struct {
	sync.RWMutex
	data map[string]*model.Metadata
}

func New() *Repository {
	return &Repository{data: map[string]*model.Metadata{}}
}

/*RWMutex нужен, когда у нас есть объект, который нельзя параллельно писать, но можно параллельно читать.
Например, стандартный тип map.
Перед записью в защищаемый мьютексом объект делается .Lock(), а вызовы .Lock() и .RLock() в других горутинах будут ждать,
пока вы не отпустите мьютекс через .Unlock().
Перед чтением защищаемого объекта делается .RLock() и только вызовы .Lock() в других горутинах блокируются,
вызовы .RLock() спокойно проходят. Когда отпускаете мьютекс через .RUnlock(), ждущие вызовы .Lock() по-очереди могут забирать мьютекс на себя.
Таких образом обеспечивается параллельное чтение объекта несколькими горутинами, что улучшает производительность.*/

// Get retrieves movie metadata for by movie id.
func (r *Repository) Get(_ context.Context, id string) (*model.Metadata, error) {
	r.RLock()
	defer r.RUnlock()
	m, ok := r.data[id]
	if !ok {
		return nil, repository.ErrNotFound
	}
	return m, nil
}

// Put adds movie metadata for a given movie id.
func (r *Repository) Put(_ context.Context, id string, metadata *model.Metadata) error {
	// TODO: fail
	//r.Lock()
	//defer r.Unlock()

	r.data[id] = metadata

	return nil
}
