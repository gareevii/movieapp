package main

import (
	"context"
	"fmt"
	"gopkg.in/yaml.v3"
	"log"
	memoryRegistry "movieexample.com/pkg/discovery/memorypackage"
	"net"
	"os"
	"time"

	"google.golang.org/grpc"
	"movieexample.com/gen"
	"movieexample.com/movie/internal/controller/movie"
	metadatagateway "movieexample.com/movie/internal/gateway/metadata/grpc"
	ratinggateway "movieexample.com/movie/internal/gateway/rating/grpc"
	grpchandler "movieexample.com/movie/internal/handler/grpc"
	"movieexample.com/pkg/discovery"
)

func main() {
	//f, err := os.Open("configs/base.yaml")
	f, err := os.Open("base.yaml")
	if err != nil {
		panic(err)
	}
	var cfg config
	if err := yaml.NewDecoder(f).Decode(&cfg); err != nil {
		panic(err)
	}
	serviceName, host, port := cfg.Service.Name, cfg.API.Host, cfg.API.Port
	log.Printf("Starting the movie service on port %d", port)
	ctx := context.Background()

	//TODO: replaced due kubernetes (consul container have to setup inside kubernetes)
	registry := memoryRegistry.NewRegistry()
	if err := registry.Register(ctx, discovery.GenerateInstanceID("metadata"), "metadata", fmt.Sprintf("%s:%d", "localhost", 8081)); err != nil {
		panic(err)
	}
	if err := registry.Register(ctx, discovery.GenerateInstanceID("rating"), "rating", fmt.Sprintf("%s:%d", "localhost", 8082)); err != nil {
		panic(err)
	}
	//----------------------------------------------------------------------------------

	/*consulHost, consulPort := cfg.Register.Consul.Host, cfg.Register.Consul.Port
	registry, err := consul.NewRegistry(fmt.Sprintf("%s:%d", consulHost, consulPort))
	if err != nil {
		panic(err)
	}*/

	instanceID := discovery.GenerateInstanceID(serviceName)
	if err := registry.Register(ctx, instanceID, serviceName, fmt.Sprintf("%s:%d", host, port)); err != nil {
		panic(err)
	}
	go func() {
		for {
			if err := registry.ReportHealthyState(instanceID, serviceName); err != nil {
				log.Println("Failed to report healthy state: " + err.Error())
			}
			time.Sleep(1 * time.Second)
		}
	}()
	defer registry.Deregister(ctx, instanceID, serviceName)

	metadataGateway := metadatagateway.New(registry)
	ratingGateway := ratinggateway.New(registry)
	ctrl := movie.New(ratingGateway, metadataGateway)
	h := grpchandler.New(ctrl)

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	srv := grpc.NewServer()
	gen.RegisterMovieServiceServer(srv, h)
	if err := srv.Serve(lis); err != nil {
		panic(err)
	}

}
