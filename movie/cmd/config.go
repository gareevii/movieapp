package main

type config struct {
	Service  serviceConfig  `yaml:"service"`
	API      apiConfig      `yaml:"api"`
	Register registerConfig `yaml:"register"`
}

type serviceConfig struct {
	Name string `yaml:"name"`
}

type apiConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type registerConfig struct {
	Consul consulConfig `yaml:"consul"`
}

type consulConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}
