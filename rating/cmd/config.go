package main

type config struct {
	Service  serviceConfig  `yaml:"service"`
	API      apiConfig      `yaml:"api"`
	Db       dbConfig       `yaml:"db"`
	Register registerConfig `yaml:"register"`
}

type serviceConfig struct {
	Name string `yaml:"name"`
}

type apiConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type dbConfig struct {
	Driver   string `yaml:"driver"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

type registerConfig struct {
	Consul consulConfig `yaml:"consul"`
}

type consulConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}
